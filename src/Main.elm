module Main exposing (..)

import Browser
import Html exposing (Html, button, div, h1, input, text)
import Html.Attributes exposing (class, placeholder, type_, value)
import Html.Events exposing (onClick, onInput)



-- MAIN


main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { num1 : String
    , num2 : String
    , operation : Maybe Operation
    , result : Maybe Float
    }


type Operation
    = Add
    | Subtract
    | Multiply
    | Divide


init : Model
init =
    { num1 = ""
    , num2 = ""
    , operation = Nothing
    , result = Nothing
    }



-- UPDATE


type Msg
    = UpdateNum1 String
    | UpdateNum2 String
    | SetOperationAndCalculate Operation


update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateNum1 num ->
            { model | num1 = num }

        UpdateNum2 num ->
            { model | num2 = num }

        SetOperationAndCalculate op ->
            let
                n1 =
                    String.toFloat model.num1

                n2 =
                    String.toFloat model.num2

                res =
                    case ( op, n1, n2 ) of
                        ( Add, Just x, Just y ) ->
                            Just (x + y)

                        ( Subtract, Just x, Just y ) ->
                            Just (x - y)

                        ( Multiply, Just x, Just y ) ->
                            Just (x * y)

                        ( Divide, Just x, Just y ) ->
                            if y /= 0 then
                                Just (x / y)

                            else
                                Nothing

                        _ ->
                            Nothing
            in
            { model | operation = Just op, result = res }



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "container mx-auto p-4" ]
        [ h1 [ class "text-xl font-bold" ] [ text "Bem-vindo ao Elm com Tailwind" ]
        , input [ type_ "text", placeholder "Número 1", value model.num1, onInput UpdateNum1, class "input-class" ] []
        , input [ type_ "text", placeholder "Número 2", value model.num2, onInput UpdateNum2, class "input-class" ] []
        , div [ class "operations" ]
            [ operationButton Add model
            , operationButton Subtract model
            , operationButton Multiply model
            , operationButton Divide model
            ]
        , div [] [ text (resultToString model.result) ]
        ]


operationButton : Operation -> Model -> Html Msg
operationButton op model =
    button [ onClick (SetOperationAndCalculate op), class "operation-button-class" ] [ text (operationToString op) ]


operationToString : Operation -> String
operationToString op =
    case op of
        Add ->
            "+"

        Subtract ->
            "-"

        Multiply ->
            "*"

        Divide ->
            "/"


resultToString : Maybe Float -> String
resultToString maybeResult =
    case maybeResult of
        Just result ->
            String.fromFloat result

        Nothing ->
            "Resultado não disponível"
