module Main exposing (..)

import Browser
import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (type_, placeholder, value, style)
import Html.Events exposing (onClick, onInput)
import Maybe

-- MAIN

main =
    Browser.sandbox { init = init, update = update, view = view }

-- MODEL

type alias Model =
    { num1 : String
    , num2 : String
    , operation : Maybe Operation
    , result : Maybe Float
    }

type Operation
    = Add
    | Subtract
    | Multiply
    | Divide

init : Model
init =
    { num1 = ""
    , num2 = ""
    , operation = Nothing
    , result = Nothing
    }

-- UPDATE

type Msg
    = UpdateNum1 String
    | UpdateNum2 String
    | SetOperationAndCalculate Operation

update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateNum1 num ->
            { model | num1 = num }

        UpdateNum2 num ->
            { model | num2 = num }

        SetOperationAndCalculate op ->
            let
                newModel = { model | operation = Just op }
                n1 = String.toFloat newModel.num1
                n2 = String.toFloat newModel.num2
                res =
                    case (newModel.operation, n1, n2) of
                        (Just Add, Just x, Just y) ->
                            Just (x + y)

                        (Just Subtract, Just x, Just y) ->
                            Just (x - y)

                        (Just Multiply, Just x, Just y) ->
                            Just (x * y)

                        (Just Divide, Just x, Just y) ->
                            if y /= 0 then
                                Just (x / y)
                            else
                                Nothing

                        _ ->
                            Nothing
            in
            { newModel | result = res }

-- VIEW

view : Model -> Html Msg
view model =
    div [ style "text-align" "center", style "padding" "20px", style "background-color" "#f0f0f0", style "border-radius" "10px", style "width" "300px", style "margin" "auto", style "box-shadow" "0 4px 8px rgba(0, 0, 0, 0.1)" ]
        [ div [ style "margin-bottom" "20px" ] 
            [ input [ type_ "text", placeholder "Number 1", onInput UpdateNum1, value model.num1, style "margin" "5px", style "border" "none", style "border-radius" "5px", style "padding" "10px", style "width" "calc(100% - 20px)" ] []
            , input [ type_ "text", placeholder "Number 2", onInput UpdateNum2, value model.num2, style "margin" "5px", style "border" "none", style "border-radius" "5px", style "padding" "10px", style "width" "calc(100% - 20px)" ] []
            ]
        , div [ style "display" "grid", style "grid-template-columns" "repeat(2, 1fr)", style "gap" "10px", style "margin-top" "10px" ]
            [ operationButton Add model
            , operationButton Subtract model
            , operationButton Multiply model
            , operationButton Divide model
            ]
        , div [ style "margin-top" "20px" ] [ text <| Maybe.withDefault "Result: " (Maybe.map String.fromFloat model.result) ]
        ]

operationButton : Operation -> Model -> Html Msg
operationButton op model =
    button [ onClick (SetOperationAndCalculate op), style "margin" "5px", style "background-color" "#fdfdfd", style "color" "#333", style "border" "none", style "border-radius" "5px", style "padding" "15px 30px", style "box-shadow" "0 2px 4px rgba(0, 0, 0, 0.1)", style "font-size" "16px", style "width" "100%" ] [ text (operationToString op) ]

operationToString : Operation -> String
operationToString op =
    case op of
        Add -> "+"
        Subtract -> "-"
        Multiply -> "*"
        Divide -> "/"
