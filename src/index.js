import { Elm } from './Main.elm';
import './styles/tailwind.css';

Elm.Main.init({
  node: document.getElementById('app')
});
