# Elm Application

This is a basic guide on how to set up and run an Elm application.

## Prerequisites

Before you begin, ensure you have installed:

- [Elm](https://guide.elm-lang.org/install/elm.html)
- [Node.js](https://nodejs.org/) (optional, for running a development server)

## Setting Up Your Elm Application

1. **Create a new directory for your project:**
   ```bash
   mkdir my-elm-app
   cd my-elm-app
   ```

## Running the Application

To view your Elm application in a browser, you have two options:

### Option 1: Using elm-reactor (For Quick Prototyping)

Start elm-reactor:

```bash
elm reactor
```

Open your browser and go to http://localhost:8000.
Navigate to src/Main.elm to see your application.

### Option 2: Using a Custom Server (e.g., Node.js)

Install elm-live:

```bash
npm install --global elm-live
```

Run your application:

```bash
elm-live src/Main.elm --open
```

This command will open your Elm application in the default web browser and refresh it automatically on file changes.

## Deploy for production

```
elm make src/Main.elm --optimize --output=main.js
```
